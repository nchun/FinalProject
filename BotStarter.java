// // Copyright 2015 theaigames.com (developers@theaigames.com)

//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at

//        http://www.apache.org/licenses/LICENSE-2.0

//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    limitations under the License.
//	
//    For the full copyright and license information, please view the LICENSE
//    file that was distributed with this source code.

package bot;
import java.util.Random;

/**
 * BotStarter class
 * 
 * Magic happens here. You should edit this file, or more specifically
 * the makeTurn() method to make your bot do more than random moves.
 * 
 * @author Jim van Eeden <jim@starapple.nl>, Joost de Meij <joost@starapple.nl>
 */

public class BotStarter {	
     Field field;
     int t = -1;
     int s = -1;
     int numMove = 1;
     private static int[][] evalTable = {{3, 4, 5, 7, 5, 4, 3}, 
                                          {4, 6, 8, 10, 8, 6, 4},
                                          {5, 8, 11, 13, 11, 8, 5}, 
                                          {5, 8, 11, 13, 11, 8, 5},
                                          {4, 6, 8, 10, 8, 6, 4},
                                          {3, 4, 5, 7, 5, 4, 3}};
     /**
      * Makes a turn. Edit this method to make your bot smarter.
      *
      * @return The column where the turn was made.
      */
     public int makeTurn(){
         int move = new Random().nextInt(7);     
         numMove++;
         return move;
     }

     public int placeInOrder() {
        for(int j = 0; j < 7; j++){
          t++;
          if(t == 7){
            t = 0;
          }
          return t;
        }
        return t;
     }


     public int startGame(){
        //The oppenent went first
        //Hard coding the turn depending on where the the first turn was made
        if(numMove < 2){
          if (findPlayer(field) == 2){
            if(field.getDisc(2,5) == 1){
              return 4;
            }
            else if (field.getDisc(4,5) == 1){
              return 2;
            }
            else{
              return 3;
            }
          }
        //Nobody has gone
          else if (findPlayer(field) == 1){
             return 3;
          }
        }
          // return negamax(field, -1000000, 0.1);
        return new Random().nextInt(4);
        }
        
     

        //Used to find what player my robot is, 1 or 2.
     public int findPlayer(Field field){
        int count = 0;
        for(int r = 0; r < 6; r++){
          {
            for(int c = 0; c < 7; c++){
              //Check to see if the oponent went first
              if(field.getDisc(c,r) == 1){
                count++;
              }
            }
          }
        }
        //The oppenent went first
        if (count == 1){
          return 2;
        }
        //Nobody has gone
        else if (count == 0){
          return 1;
        }
          return 0;
        }


        //Use Gaussian normal distribution to hard code use table
        //and give an estimate to the value of the current board
      public int evaluateF(){
        int util = 138;
        int sum = 0;
        for(int i = 0; i < field.getNrRows(); i++){
          for(int j = 0; j < field.getNrColumns(); j++){
            if(field.getDisc(i,j) == findPlayer(field)){
              sum = sum + (evalTable[i][j]);
            }
            else if(field.getDisc(i,j) == 0){
              sum = 0;
            }
            else{
              sum = sum - (evalTable[i][j]);
            }
            }
          }

          return util + sum;
        }

      
     
     
     
 	public static void main(String[] args) {
 		BotParser parser = new BotParser(new BotStarter());

 		parser.run();
 	}